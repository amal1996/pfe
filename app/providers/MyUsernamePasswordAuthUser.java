package providers;

import providers.MyUsernamePasswordAuthProvider.MySignup;

import com.feth.play.module.pa.providers.password.UsernamePasswordAuthUser;
import com.feth.play.module.pa.user.NameIdentity;
import com.feth.play.module.pa.user.Account_typeIdentity;
public class MyUsernamePasswordAuthUser extends UsernamePasswordAuthUser
		implements NameIdentity, Account_typeIdentity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final String name;
	private final String accounttype;

	public MyUsernamePasswordAuthUser(final MySignup signup) {
		super(signup.password, signup.email);
		this.name = signup.getName();
		this.accounttype = signup.getAccounttype();
	}

	/**
	 * Used for password reset only - do not use this to signup a user!
	 * @param password
	 */
	public MyUsernamePasswordAuthUser(final String password) {
		super(password, null);
		name = null;
		accounttype = null;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getAccounttype() {
		return accounttype;
	}
	
}
