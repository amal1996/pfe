package controllers;

import java.util.Date;
import java.util.List;
import views.html.errors.*;
import javax.inject.Inject;
import models.User;
import com.feth.play.module.pa.PlayAuthenticate;

import providers.MyUsernamePasswordAuthProvider;
import providers.MyUsernamePasswordAuthUser;
import models.Markets;
import models.Post;
import models.Tag;
import models.User;
import play.Routes;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;
import service.UserProvider;
import views.html.posts.*;

public class PostsController  extends Controller {
	@Inject
	FormFactory formFactory;
	
Integer Id;
private final PlayAuthenticate auth;
private final UserProvider userProvider;
@Inject
public PostsController(final PlayAuthenticate auth, final UserProvider userProvider) {
	this.auth = auth;
	this.userProvider = userProvider;
	
}


//for all posts	

	public Result index()
	{
		final User localUser = this.userProvider.getUser(session());
		//find.all().where().eq("name_startup",localUser.startup)
				List<Post>posts	= Post.find.all();
				//List<Post>postsposts.find.where().eq("name_startup",localUser.startup);
		//List<Post> posts=Post.find.all().;
				
			            	return ok(index.render(posts,userProvider,localUser));
			        

		
	}
	//to create a post

	public Result create()
	{Form<Post> postForm=formFactory.form(Post.class);
	List<Tag> tags=Tag.find.all();
	return ok(create.render(postForm,tags,userProvider));}

	//to save a post
	public Result save()
	{final User localUser = this.userProvider.getUser(session());
		Form<Post> postForm=formFactory.form(Post.class).bindFromRequest();
	List<Tag> tags=Tag.find.all();
	if(postForm.hasErrors()) {
		flash("danger","Please Correct the Form Below ");
		return badRequest(create.render(postForm,tags,userProvider));
	}

	Post post =postForm.get();
	
	post.user_id=localUser;
	post.save();
	flash("success","Post Saved Successfully");
	return redirect(routes.PostsController.index());
	}
	//to edit a post
	public Result edit(Integer id)
	{Id=id;
		Post post=Post.find.byId(id);
		if(post==null) {return notFound(views.html.errors._404.render());}
		Form<Post> postForm =formFactory.form(Post.class).fill(post);
		List<Tag> tags=Tag.find.all();
		return ok(edit.render(postForm,tags,userProvider));

	}
	//to update
	public Result update()
	{ 
		Form<Post> postForm =formFactory.form(Post.class).bindFromRequest();
		if (postForm.hasErrors())
		{
			flash("danger","Please Correct the Form Below");
			List<Tag> tags=Tag.find.all();
			return badRequest(edit.render(postForm,tags,userProvider));
		}
		Post post= postForm.get();
		Post oldPost=Post.find.byId(Id);
		if(post == null) {
			flash("danger","Post Not Found");
			return notFound();
			}
		oldPost.setBody(post.body);
		oldPost.setCreated_at(post.created_at);
		oldPost.setTitle(post.title);
		//oldPost.title=post.title;
		//oldPost.body=post.body;
		//oldPost.created_at=post.created_at;
		//oldPost.update();
		oldPost.setUpdated_at(new Date());
		oldPost.update();
		flash("success","Post updated successfully");
		return redirect(routes.PostsController.index());
	}

	//to destroy
	public Result destroy(Integer id)
	{
		final User localUser = this.userProvider.getUser(session());
		Post post=Post.find.byId(id);
		if(post==null) {
			flash("danger","Post Not Found");
			return notFound();}
		
		post.delete();
		flash("success","Post Deleted");
		return redirect(routes.PostsController.index());
		
		
	
	
	}
	//or post details
	public Result show(Integer id)
	{final User localUser = this.userProvider.getUser(session());
		Post post=Post.find.byId(id);
		if (post==null)
		{return notFound(_404.render());}

		return ok(show.render(post,userProvider,localUser));}

}







