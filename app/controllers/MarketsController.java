package controllers;

import java.util.Date;

import javax.inject.Inject;
import javax.persistence.Column;
import javax.persistence.OneToOne;

import com.feth.play.module.pa.PlayAuthenticate;

import models.Markets;
import models.Regulation;
import models.User;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;
import providers.MyUsernamePasswordAuthProvider;
import service.UserProvider;
import views.html.markets.*;
public class MarketsController extends Controller{
	@Inject
	FormFactory formFactory;
	private final PlayAuthenticate auth;

	private final MyUsernamePasswordAuthProvider provider;

	private final UserProvider userProvider;
	private Integer i;
	private double resultt ;
	private double score;
	
	@Inject
	public MarketsController (final PlayAuthenticate auth, final MyUsernamePasswordAuthProvider provider,
					   final UserProvider userProvider) {
		this.auth = auth;
		this.provider = provider;
		this.userProvider = userProvider;
	}
	public Result create()
	
	{Form<Markets> Marketsform=formFactory.form(Markets.class);
		
	return ok(create.render(Marketsform,userProvider));
	}
	

	public Result save()
	{
			Form<Markets> Marketsform=formFactory.form(Markets.class).bindFromRequest();
			Markets markets =Marketsform.get();
	//session
			final User userr = this.userProvider.getUser(session());
			Markets mark = markets.find.where().eq("user_id",userr.id).findUnique();
			i=0;
			if(markets.open_consumers_purchase_product != null) {i=i+1;}
			if(markets.percentage_sale_online != null) {i=i+1;}
			if(markets.public_markets_interest != null) {i=i+1;}
			if(markets.online_sales_cycle != null) {i=i+1;}
			if(!markets.possible_reasons.isEmpty()) {i=i+1;}
			if(markets.other_possible_reasons != null) {i=i+1;}
			if(markets.answer_interest != null) {i=i+1;}
			resultt=i*100/7;
			if(markets.percentage_sale_online != null)
			{
			score=((double)markets.open_consumers_purchase_product*20+(double)markets.percentage_sale_online)/2;
			}
			else {
				
				score=(double)markets.open_consumers_purchase_product*20/2;
			}
			
	if(Marketsform.hasErrors())
	{
		
		flash("danger","Correct the form");
		
		return redirect(routes.MarketsController.create());
		
	}
	if(mark==null)
	{
	markets.user=userr;
markets.result=resultt;
markets.score=score;
	markets.save();
	flash("success","Market Saved Successfully");
	
	return redirect(routes.Startup_ActController.create());
	}
	else {
		
		mark.setAnswer_interest(markets.answer_interest);
		mark.setOnline_sales_cycle(markets.online_sales_cycle);
		mark.setOpen_consumers_purchase_product(markets.open_consumers_purchase_product);
		mark.setPercentage_sale_online(markets.percentage_sale_online);
		mark.setPublic_markets_interest(markets.public_markets_interest);
		mark.setPossible_reasons(markets.possible_reasons);
		mark.setOther_possible_reasons(markets.other_possible_reasons);
		mark.setResult(resultt);
		mark.setScore(score);
		mark.setResult(markets.result);
		mark.setUpdated_at(new Date());
		mark.update();
		flash("success","Market Updated Successfully");
		
		return redirect(routes.Startup_ActController.create());	
	}


	}
}
