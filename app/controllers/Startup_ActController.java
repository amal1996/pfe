package controllers;
import models.User;

import java.util.Date;

import javax.inject.Inject;
import javax.persistence.OneToOne;

import com.feth.play.module.pa.PlayAuthenticate;

import models.Markets;
import models.Regulation;
import models.Startup_Act;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;
import providers.MyUsernamePasswordAuthProvider;
import service.UserProvider;
import views.html.startup_Act.*;
public class Startup_ActController extends Controller {
	@Inject
	FormFactory formFactory;
	private final PlayAuthenticate auth;

	private final MyUsernamePasswordAuthProvider provider;

	private final UserProvider userProvider;
	private Integer i;
	private double resultt ;
	private double score;
	
	@Inject
	public Startup_ActController (final PlayAuthenticate auth, final MyUsernamePasswordAuthProvider provider,
					   final UserProvider userProvider) {
		this.auth = auth;
		this.provider = provider;
		this.userProvider = userProvider;
	}
	public Result create()
	
	{Form<Startup_Act> Startup_Actform=formFactory.form(Startup_Act.class);
		
	return ok(create.render(Startup_Actform,userProvider));
	}
	
	public Result save()
	{
		
		
			Form<Startup_Act> Startup_Actform=formFactory.form(Startup_Act.class).bindFromRequest();
			Startup_Act startup_Act =Startup_Actform.get();
	//session
			final User userr = this.userProvider.getUser(session());
			Startup_Act star = startup_Act.find.where().eq("user_id",userr.id).findUnique();
	i=0;
	if(startup_Act.startup_act_support != null) {i=i+1;}
	if(startup_Act.need_of_information != null) {i=i+1;}
	if(!startup_Act.suggestion.isEmpty()) {i=i+1;}
	resultt=i*100/3;
	
	if(Startup_Actform.hasErrors())
	{
		
		flash("danger","Correct the form");
		
		return redirect(routes.Startup_ActController.create());
		
	}
	if(star==null)
	{
	startup_Act.user=userr;
	startup_Act.result=resultt;
	startup_Act.save();
	flash("success","Startup Saved Successfully");
	
	return redirect(routes.WeightingsController.create());
	}
	else
	{star.setNeed_of_information(startup_Act.need_of_information);
	star.setResult(resultt);

	
	star.setStartup_act_support(startup_Act.startup_act_support);
    star.setSuggestion(startup_Act.suggestion);
	
    star.setUpdated_at(new Date());
		star.update();
		flash("success","Startup Updated Successfully");
		
		return redirect(routes.WeightingsController.create());
	}


	}
	
	
	
}
