package controllers;

import java.util.Date;

import javax.inject.Inject;
import javax.persistence.OneToOne;

import com.feth.play.module.pa.PlayAuthenticate;

import models.Capitals;
import models.Regulation;
import models.User;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;
import providers.MyUsernamePasswordAuthProvider;
import service.UserProvider;
import models.Weightings;
import models.User;
import views.html.weightings.*;
public class WeightingsController extends Controller{

	@Inject
	FormFactory formFactory;
	private final PlayAuthenticate auth;

	private final MyUsernamePasswordAuthProvider provider;

	private final UserProvider userProvider;
	private Integer i;
	private double resultt ;

	
	@Inject
	public WeightingsController (final PlayAuthenticate auth, final MyUsernamePasswordAuthProvider provider,
					   final UserProvider userProvider) {
		this.auth = auth;
		this.provider = provider;
		this.userProvider = userProvider;
	}
	public Result create()
	
	{Form<Weightings> Weightingsform=formFactory.form(Weightings.class);
		
	return ok(create.render(Weightingsform,userProvider));
	}
	
	public Result save()
	{
		
			Form<Weightings> Weightingsform=formFactory.form(Weightings.class).bindFromRequest();
			Weightings weightings =Weightingsform.get();
	//session
			final User userr = this.userProvider.getUser(session());
			Weightings wei = weightings.find.where().eq("user_id",userr.id).findUnique();
			
			i=0;
			
			if(weightings.Regulation != null) {i=i+1;}
			if(weightings.Funding != null) {i=i+1;}
			if(weightings.Support != null) {i=i+1;}
			if(weightings.Cultures != null) {i=i+1;}
			if(weightings.Capitals != null) {i=i+1;}
			if(weightings.Market != null) {i=i+1;}
			if(weightings.Startup != null) {i=i+1;}
		
			resultt=i*100/7;
			
			if(weightings.total>100)
			{
				
				flash("danger","you cannot exceed 100% please reduce some values");
				
				return redirect(routes.WeightingsController.create());
				
			}
	
			else {
				if(wei==null)
				{
	weightings.user=userr;
	weightings.result=resultt;
	weightings.save();
	flash("success","Weightings Saved Successfully");
	
	return redirect(routes.ActivitiesController.create());}
	else
	{

		wei.setFunding(weightings.Funding);
		wei.setCapitals(weightings.Capitals);
		wei.setCultures(weightings.Cultures);
		wei.setRegulation(weightings.Regulation);
		wei.setMarket(weightings.Market);
		wei.setSupport(weightings.Support);
		wei.setStartup(weightings.Startup);
		wei.setResult(resultt);
	
		wei.setTotal(weightings.total);
		wei.setUpdated_at(new Date());
		wei.update();
		flash("success","Weightings Updated Successfully");
		
		return redirect(routes.ActivitiesController.create());
	}

	

	}
	
	}

}

