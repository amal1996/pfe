package controllers;

import java.util.Date;

import javax.inject.Inject;
import javax.persistence.Column;
import javax.persistence.OneToOne;

import com.feth.play.module.pa.PlayAuthenticate;

import models.User;
import models.Activities;
import models.Regulation;
import play.data.Form;
import play.data.FormFactory;
import play.data.format.Formats.DateTime;
import play.mvc.Controller;
import play.mvc.Result;
import providers.MyUsernamePasswordAuthProvider;
import service.UserProvider;
import views.html.activities.*;
public class ActivitiesController extends Controller{

	
	@Inject
	FormFactory formFactory;
	private final PlayAuthenticate auth;

	private final MyUsernamePasswordAuthProvider provider;

	private final UserProvider userProvider;
	private Integer i;
	private double resultt ;
	
	@Inject
	public ActivitiesController (final PlayAuthenticate auth, final MyUsernamePasswordAuthProvider provider,
					   final UserProvider userProvider) {
		this.auth = auth;
		this.provider = provider;
		this.userProvider = userProvider;
	}
	public Result create()
	
	{Form<Activities> Activitiesform=formFactory.form(Activities.class);
		
	return ok(create.render(Activitiesform,userProvider));
	}
	
	public Result save()
	{
		
			Form<Activities> Activitiesform=formFactory.form(Activities.class).bindFromRequest();
			Activities activities =Activitiesform.get();
	//session
			final User userr = this.userProvider.getUser(session());
			Activities act = activities.find.where().eq("user_id",userr.id).findUnique();
			i=0;
			if(!activities.activity_sector.isEmpty()) {i=i+1;}
			if(!activities.technology.isEmpty()) {i=i+1;}
			if(activities.other_technology != null) {i=i+1;}
			
			if(!activities.interval_revenue.isEmpty()) {i=i+1;}
			if(!activities.growth_rate.isEmpty()) {i=i+1;}
			if(!activities.startup_phase.isEmpty()) {i=i+1;}
			if(activities.other_geographical_targeting != null) {i=i+1;}
			if(activities.geographical_targeting != null) {i=i+1;}
			if(activities.enough_cover_charge != null) {i=i+1;}
			if(activities.percentage_international_revenue != null) {i=i+1;}
			if(activities.income_model != null) {i=i+1;}
			if(!activities.type_contract_employee.isEmpty()) {i=i+1;}
			if(activities.nbr_employees != null) {i=i+1;}
			if(!activities.difficulty_type.isEmpty()) {i=i+1;}
			if(!activities.innovation.isEmpty()) {i=i+1;}
			if(activities.getOther_innovation() != null) {i=i+1;}
			if(!activities.estimation.isEmpty()) {i=i+1;}
			if(activities.other_type_contract != null) {i=i+1;}
			if(!activities.urgency_survey.isEmpty()) {i=i+1;}
			if(activities.date_constitution != null) {i=i+1;}
			if(activities.research_funding != null) {i=i+1;}
			resultt=i*100/21;
			
			
			if(Activitiesform.hasErrors())
			{
				
				flash("danger","Correct the form");
				
				return redirect(routes.ActivitiesController.create());
				
			}
	
			else {
				
				if(act==null)
				{
				activities.user=userr;
				activities.result=resultt;
				activities.save();
	flash("success","Activities Saved Successfully");
	
	return redirect(routes.StartupInformationController.edit());
	
				}
				else
				{
					
					act.setActivity_sector(activities.activity_sector);
					act.setTechnology(activities.technology);
					act.setInterval_revenue(activities.interval_revenue);
					act.setGrowth_rate(activities.growth_rate);
					act.setStartup_phase(activities.startup_phase);
					act.setGeographical_targeting(activities.geographical_targeting);
					act.setEnough_cover_charge(activities.enough_cover_charge);
					act.setPercentage_international_revenue(activities.percentage_international_revenue);
					act.setIncome_model(activities.income_model);
					act.setType_contract_employee(activities.type_contract_employee);
					act.setNbr_employees(activities.nbr_employees);
					act.setDifficulty_type(activities.difficulty_type);
					act.setInnovation(activities.innovation);
					act.setEstimation(activities.estimation);
					act.setUrgency_survey(activities.urgency_survey);
					act.setResearch_funding(activities.research_funding);
					act.setDate_constitution(activities.date_constitution);
					act.setOther_technology(activities.other_technology);
					act.setOther_type_contract(activities.other_type_contract);
					act.setOther_geographical_targeting(activities.other_geographical_targeting);
                    act.setOther_innovation(activities.other_innovation);
					act.setResult(resultt);
					
					act.setUpdated_at(new Date());
					act.update();
					flash("success","Activities Updated Successfully");
					
					return redirect(routes.StartupInformationController.edit());
					
				}
	

	}

	}

	
	
	
}
