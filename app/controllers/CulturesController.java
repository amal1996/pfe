package controllers;

import java.util.Date;

import javax.inject.Inject;
import javax.persistence.OneToOne;

import com.feth.play.module.pa.PlayAuthenticate;

import models.Cultures;
import models.Regulation;
import models.User;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;
import providers.MyUsernamePasswordAuthProvider;
import service.UserProvider;
import views.html.cultures.*;

public class CulturesController extends Controller{

	
	
	
	
	@Inject
	FormFactory formFactory;
	private final PlayAuthenticate auth;

	private final MyUsernamePasswordAuthProvider provider;

	private final UserProvider userProvider;
	private Integer i;
	private double resultt ;
	private double score;
	
	@Inject
	public CulturesController (final PlayAuthenticate auth, final MyUsernamePasswordAuthProvider provider,
					   final UserProvider userProvider) {
		this.auth = auth;
		this.provider = provider;
		this.userProvider = userProvider;
	}
	public Result create()
	
	{Form<Cultures> Culturesform=formFactory.form(Cultures.class);
		
	return ok(create.render(Culturesform,userProvider));
	}
	
	public Result save()
	{
			Form<Cultures> Culturesform=formFactory.form(Cultures.class).bindFromRequest();
	Cultures cultures =Culturesform.get();
	//session
	final User userr = this.userProvider.getUser(session());
	
	Cultures cul = cultures.find.where().eq("user_id",userr.id).findUnique();
	//result
	i=0;

	if(cultures.Reaction_Entourage != null) {i=i+1;}
	if(cultures.Entourage_Experiences != null){i=i+1;}
	resultt=i*100/2;

	
	//score
	
	if(cultures.Reaction_Entourage !=null)
	{score=((double)cultures.Entourage_Experiences+(double)cultures.Reaction_Entourage)*20/2;}
	else score=(double)cultures.Entourage_Experiences*20/2;
	
	if(Culturesform.hasErrors()) {
		flash("danger","Please Correct the Form Below ");
		return redirect(routes.CulturesController.create());
	}
	if(cul==null)
	{
	cultures.user=userr;
cultures.score=score;
cultures.result=resultt;
	cultures.save();
	flash("success","Entrepreneurial culture Saved Successfully");
	
	return redirect(routes.AccompanimentsController.create());
	
	}
	else
	{cul.setEntourage_Experiences(cultures.Entourage_Experiences);
	cul.setReaction_Entourage(cultures.Reaction_Entourage);
	cul.setResult(resultt);
	cul.setScore(score);
	cul.setUpdated_at(new Date());

		cul.update();
		
		flash("success","Entrepreneurial culture Updated Successfully");
		
		return redirect(routes.AccompanimentsController.create());
	}

	}
}
