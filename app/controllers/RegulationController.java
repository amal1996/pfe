package controllers;

import java.util.Date;
import java.util.List;
import views.html.errors.*;
import java.util.Set;

import javax.inject.Inject;
import javax.persistence.Column;
import javax.persistence.OneToOne;

import com.feth.play.module.pa.PlayAuthenticate;

import models.Regulation;
import models.User;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;
import providers.MyUsernamePasswordAuthProvider;
import service.UserProvider;
import views.html.regulation.*;
import com.feth.play.module.pa.PlayAuthenticate;
import com.feth.play.module.pa.user.AuthUser;
import providers.MyUsernamePasswordAuthProvider;
import providers.MyUsernamePasswordAuthUser;
//import views.html.*;

public class RegulationController extends Controller {

	@Inject
	FormFactory formFactory;
	private final PlayAuthenticate auth;

	private final MyUsernamePasswordAuthProvider provider;

	private final UserProvider userProvider;
	private Integer i;
	private double resultt ;
	private double score;
	
	@Inject
	public RegulationController (final PlayAuthenticate auth, final MyUsernamePasswordAuthProvider provider,
					   final UserProvider userProvider) {
		this.auth = auth;
		this.provider = provider;
		this.userProvider = userProvider;
	}
	public Result create()
	{Form<Regulation> regulationform=formFactory.form(Regulation.class);
		
	return ok(create.render(regulationform,userProvider));
	}
	

	public String incitation;
	
	
	public Result save()
	{Form<Regulation> regulationform=formFactory.form(Regulation.class).bindFromRequest();
	Regulation regulation =regulationform.get();
	//session
	final User userr = this.userProvider.getUser(session());
	
	Regulation reg = regulation.find.where().eq("user_id",userr.id).findUnique();
	//result
	i=0;
	if(regulation.favorabilite_loi !=null){i=i+1;}
	if(regulation.taxe !=null){i=i+1;}
	if(regulation.favorabilite_reglementation_change != null){i=i+1;}
	if(!regulation.autre_reglementation.isEmpty()){i=i+1;}
	
	if(regulation.mesures_urgente != null){i=i+1;}
	if(regulation.corruption != null){i=i+1;}
	if(regulation.situation_payer != null){i=i+1;}
	if(regulation.incitation != null){i=i+1;}
	
	resultt=i*100/8;

	//score
score=((double)regulation.favorabilite_loi+(double)regulation.taxe+(double)regulation.incitation+(double)regulation.favorabilite_reglementation_change+(double)regulation.corruption)*20/5;

	
	
	if(regulationform.hasErrors()) {
		flash("danger","Please Correct the Form Below ");
		return redirect(routes.RegulationController.create());
	}

	else {
		if(reg == null)
		{
			regulation.user=userr;
			regulation.result=resultt;
			regulation.score=score;
	regulation.save();
	flash("success","Regulation Saved Successfully");
	return redirect(routes.FundingController.create());
		}
		else 
			
		{
			//reg.autre_reglementation=regulation.autre_reglementation;
			//reg.corruption=regulation.corruption;
			//reg.favorabilite_loi=regulation.favorabilite_loi;
			//reg.taxe=regulation.taxe;
			//reg.favorabilite_reglementation_change=regulation.favorabilite_reglementation_change;
			//reg.mesures_urgente=regulation.mesures_urgente;
		reg.setMesures_urgente(regulation.mesures_urgente);
		reg.setAutre_reglementation(regulation.autre_reglementation);
		reg.setCorruption(regulation.corruption);
		reg.setFavorabilite_loi(regulation.favorabilite_loi);
		reg.setTaxe(regulation.taxe);
		reg.setFavorabilite_reglementation_change(regulation.favorabilite_reglementation_change);
		reg.setSituation_payer(regulation.situation_payer);
		reg.setIncitation(regulation.incitation);
		
		//reg.situation_payer=regulation.situation_payer;
			//reg.incitation=regulation.incitation;
			//reg.result=regulation.result;
			//reg.score=regulation.score;
		reg.setUpdated_at(new Date());
		reg.setResult(resultt);
		reg.setScore(score);
			reg.update();
			
			flash("success","Regulation Updated Successfully");
			return redirect(routes.FundingController.create());
	
		}
		
	
	}


	
	
	
	
	
	}
	
}
