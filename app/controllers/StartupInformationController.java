package controllers;
import java.util.Date;

import javax.inject.Inject;
import javax.persistence.Column;

import com.feth.play.module.pa.PlayAuthenticate;
import providers.MyUsernamePasswordAuthProvider;
import providers.MyUsernamePasswordAuthUser;
import service.UserProvider;

import models.User;
import play.data.Form;
import play.data.FormFactory;
import play.data.format.Formats;
import play.data.format.Formats.NonEmpty;
import play.data.validation.Constraints;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.*;


public class StartupInformationController extends Controller{

	
	@Inject
	FormFactory formFactory;
	private final PlayAuthenticate auth;
	private final UserProvider userProvider;
	
	@Inject
	public StartupInformationController(final PlayAuthenticate auth, final UserProvider userProvider) {
		this.auth = auth;
		this.userProvider = userProvider;
		
	}
	
	public Result edit()
	{
		//User user=User.find.byId(id);
		//final User u = this.userProvider.getUser(session());
		final User localUser = this.userProvider.getUser(session());
	//User u=User.find.byId(localUser.id);
	
		if(localUser==null) {return notFound(views.html.errors._404.render());}
	
		Form<User> userForm =formFactory.form(User.class).fill(localUser);
		//userForm.get().
		return ok(StartupInformation.render(userForm,userProvider,localUser));
		
		
		
	}
	//to update
	public Result update()
	{
	Form<User> userForm =formFactory.form(User.class).bindFromRequest();
	if (userForm.hasErrors())
	{
	flash("danger","Please Correct the Form Below");
	return redirect(routes.StartupInformationController.edit());
	}
	User user= userForm.get();
	User oldUser=this.userProvider.getUser(session());
	if(oldUser==null) {
		
		flash("danger","Startup Not Found");
		return notFound();}
oldUser.accounttype=user.accounttype;
oldUser.name=user.name;
oldUser.phone=user.phone;
oldUser.address=user.address;
oldUser.postalcode=user.postalcode;
oldUser.city=user.city;
oldUser.state=user.state;
oldUser.country=user.country;
oldUser.email=user.email;
oldUser.fixed_Line=user.fixed_Line;
oldUser.legal_entity=user.legal_entity;
oldUser.vat=user.vat;
oldUser.registre_commerce=user.registre_commerce;
oldUser.image=user.image;
oldUser.cin=user.cin;
oldUser.startup=user.startup;
oldUser.capital_social=user.capital_social;
oldUser.member=user.member;
oldUser.organisation=user.organisation;
oldUser.setUpdated_at(new Date());
	oldUser.update();
	flash("success","Startup updated successfully");

	return redirect(routes.Application.index());
	
	
	}
}
