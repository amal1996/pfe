package controllers;
import java.util.Date;

import javax.inject.Inject;
import javax.persistence.OneToOne;

import com.feth.play.module.pa.PlayAuthenticate;

import models.Capitals;
import models.Regulation;
import models.User;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;
import providers.MyUsernamePasswordAuthProvider;
import service.UserProvider;
import views.html.capitals.*;


public class CapitalsController extends Controller {
	
	
	

	@Inject
	FormFactory formFactory;
	private final PlayAuthenticate auth;

	private final MyUsernamePasswordAuthProvider provider;

	private final UserProvider userProvider;
	private Integer i;
	private double resultt ;
	private double score;
	
	@Inject
	public CapitalsController (final PlayAuthenticate auth, final MyUsernamePasswordAuthProvider provider,
					   final UserProvider userProvider) {
		this.auth = auth;
		this.provider = provider;
		this.userProvider = userProvider;
	}
	
	public Result create()
	
	{Form<Capitals> Capitalsform=formFactory.form(Capitals.class);
		
	return ok(create.render(Capitalsform,userProvider));
	}
	

	public Result save()
	{
			Form<Capitals> Capitalsform=formFactory.form(Capitals.class).bindFromRequest();
			Capitals capitals =Capitalsform.get();
	//session
			final User userr = this.userProvider.getUser(session());
			Capitals cap = capitals.find.where().eq("user_id",userr.id).findUnique();
	i=0;
	
	if(capitals.recrutement_profils !=null) {i=i+1;}
	if(capitals.profils !=null){i=i+1;}
	if(capitals.brain_drain !=null){i=i+1;}
	resultt=i*100/3;
	
	score=((double)capitals.recrutement_profils+(double)capitals.profils+(double)capitals.brain_drain)*20/3;
	
	
	
	if(cap==null)
	{
	capitals.user=userr;
	capitals.score=score;
	capitals.result=resultt;
	capitals.save();
	flash("success","Human capital Saved Successfully");
	
	return redirect(routes.MarketsController.create());
	}
	else
	{cap.setBrain_drain(capitals.brain_drain);
	cap.setProfils(capitals.profils);
	cap.setRecrutement_profils(capitals.recrutement_profils);
	cap.setResult(resultt);
	cap.setScore(score);
	cap.setUpdated_at(new Date());
		cap.update();
		flash("success","Human capital Updated Successfully");
		
		return redirect(routes.MarketsController.create());	
	}


	}

}
