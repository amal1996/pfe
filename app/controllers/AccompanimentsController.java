package controllers;

import java.util.Date;

import javax.inject.Inject;
import javax.persistence.Column;
import javax.persistence.OneToOne;

import com.feth.play.module.pa.PlayAuthenticate;

import models.Accompaniments;
import models.Cultures;
import models.Regulation;
import models.User;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;
import providers.MyUsernamePasswordAuthProvider;
import service.UserProvider;
import views.html.accompaniments.*;
public class AccompanimentsController extends Controller{

	@Inject
	FormFactory formFactory;
	private final PlayAuthenticate auth;

	private final MyUsernamePasswordAuthProvider provider;

	private final UserProvider userProvider;
	private Integer j;
	private double resultt ;
	private double score;
	
	@Inject
	public AccompanimentsController (final PlayAuthenticate auth, final MyUsernamePasswordAuthProvider provider,
					   final UserProvider userProvider) {
		this.auth = auth;
		this.provider = provider;
		this.userProvider = userProvider;
	}
	public Result create()
	
	{Form<Accompaniments> Accompanimentsform=formFactory.form(Accompaniments.class);
		
	return ok(create.render(Accompanimentsform,userProvider));
	}
	
	public Result save()
	{
		
		
			Form<Accompaniments> Accompanimentsform=formFactory.form(Accompaniments.class).bindFromRequest();
			Accompaniments accompaniments =Accompanimentsform.get();
	//session
			final User userr = this.userProvider.getUser(session());
	
			Accompaniments acc = accompaniments.find.where().eq("user_id",userr.id).findUnique();
			//result
			j=0;
			
			if(accompaniments.programs_in_which_you_participate !=null) {j=j+1;}
			if(accompaniments.other_programs != null){j=j+1;}
			if(accompaniments.organisms_quality != null){j=j+1;}
			if(accompaniments.easy_to_find_experts != null){j=j+1;}
			if(accompaniments.qualite_infrastructure_digital != null){j=j+1;}
			if(accompaniments.internet_provider_name != null){j=j+1;}
			if(!accompaniments.other_suppliers.isEmpty()){j=j+1;}
			if(accompaniments.preferred_telephone_operator != null){j=j+1;}
			
			//if(accompaniments.payment_system !=null){j=j+1;}
		resultt=(j*100)/9;
		
score=((double)accompaniments.organisms_quality+(double)accompaniments.easy_to_find_experts+(double)accompaniments.qualite_infrastructure_digital)*20/3;
		
	if(Accompanimentsform.hasErrors()) {
		flash("danger","Please Correct the Form Below ");
		return redirect(routes.AccompanimentsController.create());
	}
	if(acc==null)
	{
	accompaniments.user=userr;
	accompaniments.result=resultt;
	accompaniments.score=score;
	accompaniments.save();
	flash("success","Support and Infrastructure Saved Successfully");
	
	return redirect(routes.CapitalsController.create());
	}
	else
	{
		acc.setEasy_to_find_experts(accompaniments.easy_to_find_experts);
		acc.setInternet_provider_name(accompaniments.internet_provider_name);
		acc.setOrganisms_quality(accompaniments.organisms_quality);
		acc.setOther_suppliers(accompaniments.other_suppliers);
		acc.setOther_programs(accompaniments.other_programs);
		acc.setPrograms_in_which_you_participate(accompaniments.programs_in_which_you_participate);
	    acc.setPayment_system(accompaniments.payment_system);
	    acc.setPreferred_telephone_operator(accompaniments.preferred_telephone_operator);
	    acc.setResult(resultt);
	    acc.setQualite_infrastructure_digital(accompaniments.qualite_infrastructure_digital);
		acc.setScore(score);
		acc.setUpdated_at(new Date());
		acc.update();
		flash("success","Support and Infrastructure Updated Successfully");
		
		return redirect(routes.CapitalsController.create());
	}

	
		
	}

}

	
	

