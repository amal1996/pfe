package controllers;

import java.util.List;

import javax.inject.Inject;
import com.feth.play.module.pa.PlayAuthenticate;
import providers.MyUsernamePasswordAuthProvider;
import providers.MyUsernamePasswordAuthUser;
import models.Tag;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;
import service.UserProvider;
import views.html.tags.*;
public class TagsController extends Controller {
	@Inject
	FormFactory formFactory;
	private final PlayAuthenticate auth;
	private final UserProvider userProvider;
	@Inject
	public TagsController(final PlayAuthenticate auth, final UserProvider userProvider) {
		this.auth = auth;
		this.userProvider = userProvider;
		
	}
	public Result create()
	{Form<Tag> tagForm=formFactory.form(Tag.class);

	return ok(create.render(tagForm,userProvider));}

	//to save a tag
	public Result save()
	{
		Form<Tag> tagForm=formFactory.form(Tag.class).bindFromRequest();
		if(tagForm.hasErrors()) {
			flash("danger","Please Correct the Form Below ");
			return badRequest(create.render(tagForm,userProvider));
		}

		Tag tag =tagForm.get();
		tag.save();
		flash("success","Tag Save Successfully  "+tag.value+"  !!");
		//return ok("value"+tag.id+""+tag.value);
	return redirect(routes.TagsController.index());


	}

	public Result index()
	{
		List<Tag> tags=Tag.find.all();

		return ok(index.render(tags,userProvider));
	}


}
