package controllers;


import java.util.Date;
import java.util.List;
import views.html.errors.*;
import java.util.Set;

import javax.inject.Inject;
import javax.persistence.Column;
import javax.persistence.OneToOne;

import com.feth.play.module.pa.PlayAuthenticate;

import models.Funding;
import models.Regulation;
import models.User;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;
import providers.MyUsernamePasswordAuthProvider;
import service.UserProvider;
import views.html.funding.*;
//import views.html.*;

public class FundingController extends Controller {

	@Inject
	FormFactory formFactory;
	private final PlayAuthenticate auth;

	private final MyUsernamePasswordAuthProvider provider;

	private final UserProvider userProvider;
	private Integer f;
	private double resultt ;
	private double score;
	
	@Inject
	public FundingController (final PlayAuthenticate auth, final MyUsernamePasswordAuthProvider provider,
					   final UserProvider userProvider) {
		this.auth = auth;
		this.provider = provider;
		this.userProvider = userProvider;
	}
	public Result create()
	
	{Form<Funding> Fundingform=formFactory.form(Funding.class);
		
	return ok(create.render(Fundingform,userProvider));
	}
	

	
	public Result save()
	{
		
	Form<Funding> Fundingform=formFactory.form(Funding.class).bindFromRequest();
	Funding funding =Fundingform.get();
	//session
	final User userr = this.userProvider.getUser(session());
	Funding Fun = funding.find.where().eq("user_id",userr.id).findUnique();
	//result
	f=0;
	if(!funding.financement_lancement_startup.isEmpty()) {f=f+1;}
	if(funding.autre_financement_lancement_startup != null){f=f+1;}
	if(!funding.organisme_participant.isEmpty()){f=f+1;}
	if(funding.autre_organisme_participant != null){f=f+1;}
	if(!funding.duree_contact_realisation.isEmpty()){f=f+1;}
	if(funding.qualite_financement != null){f=f+1;}
	if(!funding.changement_urgent.isEmpty()){f=f+1;}
   if(funding.autre_changement_urgent != null){f=f+1;}
   resultt=f*100/8;
  
    
    //score
  
    score=((double)funding.qualite_financement)*20;
   
    
	if(Fundingform.hasErrors()) {
		flash("danger","Please Correct the Form Below ");
		return redirect(routes.FundingController.create());
	}
if(Fun==null)
{
	funding.user=userr;
	funding.result=resultt;
	funding.score=score;
	funding.save();
	flash("success","Funding Saved Successfully");
	
	return redirect(routes.CulturesController.create());

}
else
{Fun.setAutre_changement_urgent(funding.autre_changement_urgent);
Fun.setAutre_financement_lancement_startup(funding.autre_financement_lancement_startup);
Fun.setAutre_organisme_participant(funding.autre_organisme_participant);	
Fun.setDuree_contact_realisation(funding.duree_contact_realisation);
Fun.setOrganisme_participant(funding.organisme_participant);	
Fun.setQualite_financement(funding.qualite_financement);
Fun.setChangement_urgent(funding.changement_urgent);
Fun.setFinancement_lancement_startup(funding.financement_lancement_startup);

Fun.setUpdated_at(new Date());
Fun.setResult(resultt);
Fun.setScore(score);
Fun.update();
flash("success","Funding Updated Successfully");

return redirect(routes.CulturesController.create());

}
	}
	
	
}
