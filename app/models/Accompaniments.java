package models;
import models.User;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import com.avaje.ebean.Model;
import com.avaje.ebean.Model.Finder;

import play.data.validation.Constraints;
@Entity
public class Accompaniments extends Model{
	private static final long serialVersionUID = 1L;

	@Id
	public Long id;
@Column(length=255)
public String programs_in_which_you_participate;
public String other_programs;
public Integer organisms_quality;
public Integer easy_to_find_experts;
public Integer qualite_infrastructure_digital;
@Column(length=255)
public String internet_provider_name;
public String other_suppliers;
@Column(length=255)
public String preferred_telephone_operator;
@Column(length=255)
public String payment_system;
@Column(columnDefinition = "timestamp default now()")
public Date created_at = new Date();

@Column(columnDefinition = "timestamp default now()")
public Date updated_at = new Date();
public Date getCreated_at() {
	return created_at;
}
public void setCreated_at(Date created_at) {
	this.created_at = created_at;
}
public Date getUpdated_at() {
	return updated_at;
}
public void setUpdated_at(Date updated_at) {
	this.updated_at = updated_at;
}
@OneToOne

public User user;
public double result;
public double score;
public Accompaniments(Long id, String programs_in_which_you_participate, String other_programs,
		Integer organisms_quality, Integer easy_to_find_experts, Integer qualite_infrastructure_digital,
		String internet_provider_name, String other_suppliers, String preferred_telephone_operator,
		String payment_system, double result, double score) {
	super();
	this.id = id;
	this.programs_in_which_you_participate = programs_in_which_you_participate;
	this.other_programs = other_programs;
	this.organisms_quality = organisms_quality;
	this.easy_to_find_experts = easy_to_find_experts;
	this.qualite_infrastructure_digital = qualite_infrastructure_digital;
	this.internet_provider_name = internet_provider_name;
	this.other_suppliers = other_suppliers;
	this.preferred_telephone_operator = preferred_telephone_operator;
	this.payment_system = payment_system;
	this.result = result;
	this.score = score;
}
public Accompaniments() {
	
}
public String getPrograms_in_which_you_participate() {
	return programs_in_which_you_participate;
}
public void setPrograms_in_which_you_participate(String programs_in_which_you_participate) {
	this.programs_in_which_you_participate = programs_in_which_you_participate;
}
public String getOther_programs() {
	return other_programs;
}
public void setOther_programs(String other_programs) {
	this.other_programs = other_programs;
}
public Integer getOrganisms_quality() {
	return organisms_quality;
}
public void setOrganisms_quality(Integer organisms_quality) {
	this.organisms_quality = organisms_quality;
}
public Integer getEasy_to_find_experts() {
	return easy_to_find_experts;
}
public void setEasy_to_find_experts(Integer easy_to_find_experts) {
	this.easy_to_find_experts = easy_to_find_experts;
}
public Integer getQualite_infrastructure_digital() {
	return qualite_infrastructure_digital;
}
public void setQualite_infrastructure_digital(Integer qualite_infrastructure_digital) {
	this.qualite_infrastructure_digital = qualite_infrastructure_digital;
}
public String getInternet_provider_name() {
	return internet_provider_name;
}
public void setInternet_provider_name(String internet_provider_name) {
	this.internet_provider_name = internet_provider_name;
}
public String getOther_suppliers() {
	return other_suppliers;
}
public void setOther_suppliers(String other_suppliers) {
	this.other_suppliers = other_suppliers;
}
public String getPreferred_telephone_operator() {
	return preferred_telephone_operator;
}
public void setPreferred_telephone_operator(String preferred_telephone_operator) {
	this.preferred_telephone_operator = preferred_telephone_operator;
}
public String getPayment_system() {
	return payment_system;
}
public void setPayment_system(String payment_system) {
	this.payment_system = payment_system;
}
public double getResult() {
	return result;
}
public void setResult(double result) {
	this.result = result;
}
public double getScore() {
	return score;
}
public void setScore(double score) {
	this.score = score;
}
public static Model.Finder<Long,Accompaniments> find = new Model.Finder<Long,Accompaniments>(Accompaniments.class);

}
