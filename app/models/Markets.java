package models;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.avaje.ebean.Model;
@Entity
public class Markets extends Model{
	private static final long serialVersionUID = 1L;

	@Id
	public Long id;
public Integer open_consumers_purchase_product;
public Integer percentage_sale_online;
public Integer public_markets_interest;
public Integer online_sales_cycle;
@Column(length=255)
public String possible_reasons;
public String other_possible_reasons;
public String answer_interest ;
@Column(columnDefinition = "timestamp default now()")
public Date created_at = new Date();

@Column(columnDefinition = "timestamp default now()")
public Date updated_at = new Date();
public Date getCreated_at() {
	return created_at;
}
public void setCreated_at(Date created_at) {
	this.created_at = created_at;
}
public Date getUpdated_at() {
	return updated_at;
}
public void setUpdated_at(Date updated_at) {
	this.updated_at = updated_at;
}
@OneToOne

public User user;
public double result;
public double score;
public Integer getOpen_consumers_purchase_product() {
	return open_consumers_purchase_product;
}
public void setOpen_consumers_purchase_product(Integer open_consumers_purchase_product) {
	this.open_consumers_purchase_product = open_consumers_purchase_product;
}
public Integer getPercentage_sale_online() {
	return percentage_sale_online;
}
public void setPercentage_sale_online(Integer percentage_sale_online) {
	this.percentage_sale_online = percentage_sale_online;
}
public Integer getPublic_markets_interest() {
	return public_markets_interest;
}
public void setPublic_markets_interest(Integer public_markets_interest) {
	this.public_markets_interest = public_markets_interest;
}
public Integer getOnline_sales_cycle() {
	return online_sales_cycle;
}
public void setOnline_sales_cycle(Integer online_sales_cycle) {
	this.online_sales_cycle = online_sales_cycle;
}
public String getPossible_reasons() {
	return possible_reasons;
}
public void setPossible_reasons(String possible_reasons) {
	this.possible_reasons = possible_reasons;
}
public String getOther_possible_reasons() {
	return other_possible_reasons;
}
public void setOther_possible_reasons(String other_possible_reasons) {
	this.other_possible_reasons = other_possible_reasons;
}
public String getAnswer_interest() {
	return answer_interest;
}
public void setAnswer_interest(String answer_interest) {
	this.answer_interest = answer_interest;
}
public double getResult() {
	return result;
}
public void setResult(double result) {
	this.result = result;
}
public double getScore() {
	return score;
}
public void setScore(double score) {
	this.score = score;
}
public Markets(Long id, Integer open_consumers_purchase_product, Integer percentage_sale_online,
		Integer public_markets_interest, Integer online_sales_cycle, String possible_reasons,
		String other_possible_reasons, String answer_interest, User user, double result, double score) {
	super();
	this.id = id;
	this.open_consumers_purchase_product = open_consumers_purchase_product;
	this.percentage_sale_online = percentage_sale_online;
	this.public_markets_interest = public_markets_interest;
	this.online_sales_cycle = online_sales_cycle;
	this.possible_reasons = possible_reasons;
	this.other_possible_reasons = other_possible_reasons;
	this.answer_interest = answer_interest;
	this.user = user;
	this.result = result;
	this.score = score;
}
public Markets() {
	super();
}
public static Model.Finder<Long,Markets> find = new Model.Finder<Long,Markets>(Markets.class);
}
