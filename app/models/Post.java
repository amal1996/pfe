package models;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import com.avaje.ebean.Model;
@Entity
public class Post extends Model {
	@Id
	public Integer id;
	public String title;
	public String body;

	@ManyToOne

	public User user_id;
	public User getUser() {
		return user_id;
	}
	public void setUser(User user_id) {
		this.user_id = user_id;
	}


	public String tags ;
	public String getTags() {
		return tags;
	}
	public void setTags(String tags) {
		this.tags = tags;
	}


	@Column(columnDefinition = "timestamp default now()")
	public Date created_at = new Date();
	@Column(columnDefinition = "timestamp default now()")
	public Date updated_at = new Date();
	
	public Date getUpdated_at() {
		return updated_at;
	}
	public void setUpdated_at(Date updated_at) {
		this.updated_at = updated_at;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public Date getCreated_at() {
		
		return created_at;
	}

	public void setCreated_at(Date created_at) {
		this.created_at = created_at;
	}





	public User getUser_id() {
		return user_id;
	}
	public void setUser_id(User user_id) {
		this.user_id = user_id;
	}


	public static Finder<Integer,Post> find =new Finder<>(Post.class);




}
