package models;
import models.User;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.avaje.ebean.Model;
import com.avaje.ebean.Model.Finder;

import play.data.validation.Constraints;
@Entity
public class Funding extends Model{
	private static final long serialVersionUID = 1L;

	@Id
	public Long id;
@Column(length=255)
public String financement_lancement_startup;
public String autre_financement_lancement_startup;
@Column(length=255)
public String organisme_participant;

public String autre_organisme_participant;
@Column(length=255)
public String duree_contact_realisation;
public Integer qualite_financement;
@Column(length=255)
public String changement_urgent;
@Column(columnDefinition = "timestamp default now()")
public Date created_at = new Date();

@Column(columnDefinition = "timestamp default now()")
public Date updated_at = new Date();
public Date getCreated_at() {
	return created_at;
}
public void setCreated_at(Date created_at) {
	this.created_at = created_at;
}
public Date getUpdated_at() {
	return updated_at;
}
public void setUpdated_at(Date updated_at) {
	this.updated_at = updated_at;
}
@OneToOne

public User user;
public String autre_changement_urgent;
public double result;
public double score;
public double getScore() {
	return score;
}
public void setScore(double score) {
	this.score = score;
}
public Long getId() {
	return id;
}
public void setId(Long id) {
	this.id = id;
}
public String getFinancement_lancement_startup() {
	return financement_lancement_startup;
}
public void setFinancement_lancement_startup(String financement_lancement_startup) {
	this.financement_lancement_startup = financement_lancement_startup;
}
public String getAutre_financement_lancement_startup() {
	return autre_financement_lancement_startup;
}
public void setAutre_financement_lancement_startup(String autre_financement_lancement_startup) {
	this.autre_financement_lancement_startup = autre_financement_lancement_startup;
}
public String getOrganisme_participant() {
	return organisme_participant;
}
public void setOrganisme_participant(String organisme_participant) {
	this.organisme_participant = organisme_participant;
}
public String getAutre_organisme_participant() {
	return autre_organisme_participant;
}
public void setAutre_organisme_participant(String autre_organisme_participant) {
	this.autre_organisme_participant = autre_organisme_participant;
}
public String getDuree_contact_realisation() {
	return duree_contact_realisation;
}
public void setDuree_contact_realisation(String duree_contact_realisation) {
	this.duree_contact_realisation = duree_contact_realisation;
}
public Integer getQualite_financement() {
	return qualite_financement;
}
public void setQualite_financement(Integer qualite_financement) {
	this.qualite_financement = qualite_financement;
}
public String getChangement_urgent() {
	return changement_urgent;
}
public void setChangement_urgent(String changement_urgent) {
	this.changement_urgent = changement_urgent;
}
public String getAutre_changement_urgent() {
	return autre_changement_urgent;
}
public void setAutre_changement_urgent(String autre_changement_urgent) {
	this.autre_changement_urgent = autre_changement_urgent;
}
public double getResult() {
	return result;
}
public void setResult(double result) {
	this.result = result;
}
public Funding(Long id, String financement_lancement_startup, String autre_financement_lancement_startup,
		String organisme_participant, String autre_organisme_participant, String duree_contact_realisation,
		Integer qualite_financement, String changement_urgent, String autre_changement_urgent, double result, double score) {
	super();
	this.id = id;
	this.financement_lancement_startup = financement_lancement_startup;
	this.autre_financement_lancement_startup = autre_financement_lancement_startup;
	this.organisme_participant = organisme_participant;
	this.autre_organisme_participant = autre_organisme_participant;
	this.duree_contact_realisation = duree_contact_realisation;
	this.qualite_financement = qualite_financement;
	this.changement_urgent = changement_urgent;
	this.autre_changement_urgent = autre_changement_urgent;
	this.result = result;
	this.score= score;
}
public Funding() {
	
}
public static Model.Finder<Long,Funding> find = new Model.Finder<Long,Funding>(Funding.class);




}
