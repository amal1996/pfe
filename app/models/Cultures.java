package models;
import models.User;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.avaje.ebean.Model;
import com.avaje.ebean.Model.Finder;

import play.data.validation.Constraints;
@Entity
public class Cultures extends Model{

	private static final long serialVersionUID = 1L;

	@Id
	public Long id;
	public Integer Reaction_Entourage;
	public Integer Entourage_Experiences;
	@Column(columnDefinition = "timestamp default now()")
	public Date created_at = new Date();

	@Column(columnDefinition = "timestamp default now()")
	public Date updated_at = new Date();
	public Date getCreated_at() {
		return created_at;
	}

	public void setCreated_at(Date created_at) {
		this.created_at = created_at;
	}

	public Date getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(Date updated_at) {
		this.updated_at = updated_at;
	}

	@OneToOne
	
	public User user;
	public double result;
	
	public double score;
	
	public static Finder<Long, Cultures> find = new Finder<Long,Cultures>(Cultures.class);

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getReaction_Entourage() {
		return Reaction_Entourage;
	}

	public void setReaction_Entourage(Integer reaction_Entourage) {
		Reaction_Entourage = reaction_Entourage;
	}

	public Integer getEntourage_Experiences() {
		return Entourage_Experiences;
	}

	public void setEntourage_Experiences(Integer entourage_Experiences) {
		Entourage_Experiences = entourage_Experiences;
	}

	public double getResult() {
		return result;
	}

	public void setResult(double result) {
		this.result = result;
	}

	public double getScore() {
		return score;
	}

	public void setScore(double score) {
		this.score = score;
	}

	public Cultures(Long id, Integer reaction_Entourage, Integer entourage_Experiences, double result,
			double score) {
		super();
		this.id = id;
		Reaction_Entourage = reaction_Entourage;
		Entourage_Experiences = entourage_Experiences;
		this.result = result;
		this.score = score;
	}

	public Cultures() {
		
	}
		
	
	
	
}
