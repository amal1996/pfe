package models;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.avaje.ebean.Model;
import com.avaje.ebean.Model.Finder;
@Entity
public class Weightings extends Model{
	private static final long serialVersionUID = 1L;

	@Id
	public Long id;
public Integer Regulation;
public Integer Funding;
public Integer Support;
public Integer Cultures;
public Integer Capitals;
public Integer Market;
public Integer Startup;
@Column(columnDefinition = "timestamp default now()")
public Date created_at = new Date();

@Column(columnDefinition = "timestamp default now()")
public Date updated_at = new Date();
public Date getCreated_at() {
	return created_at;
}
public void setCreated_at(Date created_at) {
	this.created_at = created_at;
}
public Date getUpdated_at() {
	return updated_at;
}
public void setUpdated_at(Date updated_at) {
	this.updated_at = updated_at;
}
@OneToOne

public User user;
public double result;

public double total;
public Integer getRegulation() {
	return Regulation;
}
public void setRegulation(Integer regulation) {
	Regulation = regulation;
}
public Integer getFunding() {
	return Funding;
}
public void setFunding(Integer funding) {
	Funding = funding;
}
public Integer getSupport() {
	return Support;
}
public void setSupport(Integer support) {
	Support = support;
}
public Integer getCultures() {
	return Cultures;
}
public void setCultures(Integer cultures) {
	Cultures = cultures;
}
public Integer getCapitals() {
	return Capitals;
}
public void setCapitals(Integer capitals) {
	Capitals = capitals;
}
public Integer getMarket() {
	return Market;
}
public void setMarket(Integer market) {
	Market = market;
}
public Integer getStartup() {
	return Startup;
}
public void setStartup(Integer startup) {
	Startup = startup;
}
public double getResult() {
	return result;
}
public void setResult(double result) {
	this.result = result;
}

public double getTotal() {
	return total;
}
public void setTotal(double total) {
	this.total = total;
}
public Weightings(Integer regulation, Integer funding, Integer support, Integer cultures, Integer capitals,
		Integer market, Integer startup, double result, double total) {
	super();
	Regulation = regulation;
	Funding = funding;
	Support = support;
	Cultures = cultures;
	Capitals = capitals;
	Market = market;
	Startup = startup;
	this.result = result;

	this.total = total;
}
public Weightings() {
	super();
}
public static Finder<Long, Weightings> find = new Finder<Long,Weightings>(Weightings.class);
	
	
	
}
