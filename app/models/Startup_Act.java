package models;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.avaje.ebean.Model;
@Entity
public class Startup_Act extends Model {
	private static final long serialVersionUID = 1L;

	@Id
	public Long id;
public String startup_act_support;
public String need_of_information;
public String suggestion;
@Column(columnDefinition = "timestamp default now()")
public Date created_at = new Date();

public Date getCreated_at() {
	return created_at;
}
public void setCreated_at(Date created_at) {
	this.created_at = created_at;
}
public Date getUpdated_at() {
	return updated_at;
}
public void setUpdated_at(Date updated_at) {
	this.updated_at = updated_at;
}

@Column(columnDefinition = "timestamp default now()")
public Date updated_at = new Date();
@OneToOne

public User user;
public double result;

public Startup_Act(Long id, String startup_act_support, String need_of_information, String suggestion, User user,
		double result) {
	super();
	this.id = id;
	this.startup_act_support = startup_act_support;
	this.need_of_information = need_of_information;
	this.suggestion = suggestion;
	this.user = user;
	this.result = result;

}
public Startup_Act() {
	super();
}
public String isStartup_act_support() {
	return startup_act_support;
}
public void setStartup_act_support(String startup_act_support) {
	this.startup_act_support = startup_act_support;
}
public String isNeed_of_information() {
	return need_of_information;
}
public void setNeed_of_information(String need_of_information) {
	this.need_of_information = need_of_information;
}
public String getSuggestion() {
	return suggestion;
}
public void setSuggestion(String suggestion) {
	this.suggestion = suggestion;
}
public double getResult() {
	return result;
}
public void setResult(double result) {
	this.result = result;
}



public static Model.Finder<Long,Startup_Act> find = new Model.Finder<Long,Startup_Act>(Startup_Act.class);
}
