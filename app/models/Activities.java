package models;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.avaje.ebean.Model;
import com.avaje.ebean.Model.Finder;

import play.data.format.Formats.DateTime;
@Entity
public class Activities extends Model {
	private static final long serialVersionUID = 1L;

	@Id
	public Long id;
@Column(length=255)
public String activity_sector;
@Column(length=255)
public String technology;
public String other_technology;
@Column(length=255)
public String interval_revenue;
@Column(length=255)
public String growth_rate;
@Column(length=255)
public String startup_phase;
@Column(length=255)
public String geographical_targeting;
@Column(length=255)
public String enough_cover_charge;
public Integer percentage_international_revenue;
@Column(length=255)
public String income_model;
@Column(length=255)
public String type_contract_employee;
public Integer nbr_employees;
@Column(length=255)
public String difficulty_type;
public String innovation;
@Column(length=255)
public String estimation;
@Column(length=255)
public String urgency_survey;
public String research_funding;
@DateTime(pattern = "mm-dd-yyyy")
public Date date_constitution ;
public String other_geographical_targeting;
public String other_type_contract;
public String other_innovation;
@Column(columnDefinition = "timestamp default now()")
public Date created_at = new Date();

@Column(columnDefinition = "timestamp default now()")
public Date updated_at = new Date();
public Date getCreated_at() {
	return created_at;
}

public void setCreated_at(Date created_at) {
	this.created_at = created_at;
}

public Date getUpdated_at() {
	return updated_at;
}

public void setUpdated_at(Date updated_at) {
	this.updated_at = updated_at;
}
@OneToOne

public User user;
public double result;

public String getActivity_sector() {
	return activity_sector;
}

public String getUrgency_survey() {
	return urgency_survey;
}

public void setUrgency_survey(String urgency_survey) {
	this.urgency_survey = urgency_survey;
}

public String isResearch_funding() {
	return research_funding;
}

public void setResearch_funding(String research_funding) {
	this.research_funding = research_funding;
}

public void setActivity_sector(String activity_sector) {
	this.activity_sector = activity_sector;
}
public String getTechnology() {
	return technology;
}
public void setTechnology(String technology) {
	this.technology = technology;
}
public String getOther_technology() {
	return other_technology;
}
public void setOther_technology(String other_technology) {
	this.other_technology = other_technology;
}
public String getInterval_revenue() {
	return interval_revenue;
}
public void setInterval_revenue(String interval_revenue) {
	this.interval_revenue = interval_revenue;
}
public String getGrowth_rate() {
	return growth_rate;
}
public void setGrowth_rate(String growth_rate) {
	this.growth_rate = growth_rate;
}
public String getStartup_phase() {
	return startup_phase;
}
public void setStartup_phase(String startup_phase) {
	this.startup_phase = startup_phase;
}
public String getGeographical_targeting() {
	return geographical_targeting;
}
public void setGeographical_targeting(String geographical_targeting) {
	this.geographical_targeting = geographical_targeting;
}
public String getEnough_cover_charge() {
	return enough_cover_charge;
}
public void setEnough_cover_charge(String enough_cover_charge) {
	this.enough_cover_charge = enough_cover_charge;
}
public Integer getPercentage_international_revenue() {
	return percentage_international_revenue;
}
public void setPercentage_international_revenue(Integer percentage_international_revenue) {
	this.percentage_international_revenue = percentage_international_revenue;
}
public String getIncome_model() {
	return income_model;
}
public void setIncome_model(String income_model) {
	this.income_model = income_model;
}
public String getType_contract_employee() {
	return type_contract_employee;
}
public void setType_contract_employee(String type_contract_employee) {
	this.type_contract_employee = type_contract_employee;
}
public Integer getNbr_employees() {
	return nbr_employees;
}
public void setNbr_employees(Integer nbr_employees) {
	this.nbr_employees = nbr_employees;
}
public String getDifficulty_type() {
	return difficulty_type;
}
public void setDifficulty_type(String difficulty_type) {
	this.difficulty_type = difficulty_type;
}
public String getInnovation() {
	return innovation;
}
public void setInnovation(String innovation) {
	this.innovation = innovation;
}
public String getEstimation() {
	return estimation;
}
public void setEstimation(String estimation) {
	this.estimation = estimation;
}
public Date getDate_constitution() {
	return date_constitution;
}
public void setDate_constitution(Date date_constitution) {
	this.date_constitution = date_constitution;
}
public String getOther_geographical_targeting() {
	return other_geographical_targeting;
}
public void setOther_geographical_targeting(String other_geographical_targeting) {
	this.other_geographical_targeting = other_geographical_targeting;
}
public String getOther_type_contract() {
	return other_type_contract;
}
public void setOther_type_contract(String other_type_contract) {
	this.other_type_contract = other_type_contract;
}
public String getOther_innovation() {
	return other_innovation;
}
public void setOther_innovation(String other_innovation) {
	this.other_innovation = other_innovation;
}
public double getResult() {
	return result;
}
public void setResult(double result) {
	this.result = result;
}

public Activities(String activity_sector, String technology, String other_technology, String interval_revenue,
		String growth_rate, String startup_phase, String geographical_targeting, String enough_cover_charge,
		Integer percentage_international_revenue, String income_model, String type_contract_employee,
		Integer nbr_employees, String difficulty_type, String innovation, String estimation, Date date_constitution,
		String other_geographical_targeting, String other_type_contract, String other_innovation, User user,
		double result) {
	super();
	this.activity_sector = activity_sector;
	this.technology = technology;
	this.other_technology = other_technology;
	this.interval_revenue = interval_revenue;
	this.growth_rate = growth_rate;
	this.startup_phase = startup_phase;
	this.geographical_targeting = geographical_targeting;
	this.enough_cover_charge = enough_cover_charge;
	this.percentage_international_revenue = percentage_international_revenue;
	this.income_model = income_model;
	this.type_contract_employee = type_contract_employee;
	this.nbr_employees = nbr_employees;
	this.difficulty_type = difficulty_type;
	this.innovation = innovation;
	this.estimation = estimation;
	this.date_constitution = date_constitution;
	this.other_geographical_targeting = other_geographical_targeting;
	this.other_type_contract = other_type_contract;
	this.other_innovation = other_innovation;
	this.user = user;
	this.result = result;
	
}
public Activities() {
	super();
}
public static Finder<Long, Activities> find = new Finder<Long,Activities>(Activities.class);
}
