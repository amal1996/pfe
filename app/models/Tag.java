package models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import com.avaje.ebean.Model;
import com.avaje.ebean.Model.Finder;

@Entity
public class Tag  extends Model {
		@Id
		public Integer id;
		public String value;
		@Column(columnDefinition = "timestamp default now()")
		public Date created_at = new Date();

		public static Finder<Integer,Tag> find =new Finder<>(Tag.class);
}
