package models;
import models.User;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.avaje.ebean.Model;
import com.avaje.ebean.Model.Finder;
@Entity
public class Capitals extends Model{
	private static final long serialVersionUID = 1L;

	@Id
	public Long id;
public Integer recrutement_profils;
public Integer profils;
public Integer brain_drain;
@Column(columnDefinition = "timestamp default now()")
public Date created_at = new Date();

@Column(columnDefinition = "timestamp default now()")
public Date updated_at = new Date();
public Date getCreated_at() {
	return created_at;
}
public void setCreated_at(Date created_at) {
	this.created_at = created_at;
}
public Date getUpdated_at() {
	return updated_at;
}
public void setUpdated_at(Date updated_at) {
	this.updated_at = updated_at;
}
@OneToOne

public User user;
public double result;
public double score;
public Capitals(Integer recrutement_profils, Integer profils, Integer brain_drain, double result, double score) {
	super();
	this.recrutement_profils = recrutement_profils;
	this.profils = profils;
	this.brain_drain = brain_drain;
	this.result = result;
	this.score = score;
}
public Capitals() {
	
}
public Integer getRecrutement_profils() {
	return recrutement_profils;
}
public void setRecrutement_profils(Integer recrutement_profils) {
	this.recrutement_profils = recrutement_profils;
}
public Integer getProfils() {
	return profils;
}
public void setProfils(Integer profils) {
	this.profils = profils;
}
public Integer getBrain_drain() {
	return brain_drain;
}
public void setBrain_drain(Integer brain_drain) {
	this.brain_drain = brain_drain;
}
public double getResult() {
	return result;
}
public void setResult(double result) {
	this.result = result;
}
public double getScore() {
	return score;
}
public void setScore(double score) {
	this.score = score;
}
public static Finder<Long, Capitals> find = new Finder<Long,Capitals>(Capitals.class);
}
