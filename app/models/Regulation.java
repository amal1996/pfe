package models;
import models.User;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.avaje.ebean.Model;
import com.avaje.ebean.Model.Finder;

import play.data.validation.Constraints;
@Entity
public class Regulation extends Model{
	private static final long serialVersionUID = 1L;

	@Id
	public Long id;
	public Integer favorabilite_loi;
	public Integer taxe;
	public Integer favorabilite_reglementation_change;
	public String autre_reglementation;
	@Column(length=255)
	public String mesures_urgente;
	public Integer corruption;
	public String situation_payer;
	@Column(length=255)
	public Integer incitation;
	@Column(columnDefinition = "timestamp default now()")
	public Date created_at = new Date();

	@Column(columnDefinition = "timestamp default now()")
	public Date updated_at = new Date();
	 public Date getCreated_at() {
		return created_at;
	}
	public void setCreated_at(Date created_at) {
		this.created_at = created_at;
	}
	public Date getUpdated_at() {
		return updated_at;
	}
	public void setUpdated_at(Date updated_at) {
		this.updated_at = updated_at;
	}

	@OneToOne
	   
	    public User user;
	

	public double result;
	public double score;
	public double getScore() {
		return score;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public void setScore(double score) {
		this.score = score;
	}
	
	public Regulation() {
		
	}

	public Regulation(Long id, Integer favorabilite_loi, Integer taxe, Integer favorabilite_reglementation_change,
			String autre_reglementation, String mesures_urgente, Integer corruption, String situation_payer,
			Integer incitation, User user, Double result, Double score) {
		super();
		this.id = id;
		this.favorabilite_loi = favorabilite_loi;
		this.taxe = taxe;
		this.favorabilite_reglementation_change = favorabilite_reglementation_change;
		this.autre_reglementation = autre_reglementation;
		this.mesures_urgente = mesures_urgente;
		this.corruption = corruption;
		this.situation_payer = situation_payer;
		this.incitation = incitation;
		
		this.result = result;
		this.score= score;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getFavorabilite_loi() {
		return favorabilite_loi;
	}

	public void setFavorabilite_loi(Integer favorabilite_loi) {
		this.favorabilite_loi = favorabilite_loi;
	}

	public Integer getTaxe() {
		return taxe;
	}

	public void setTaxe(Integer taxe) {
		this.taxe = taxe;
	}

	public Integer getFavorabilite_reglementation_change() {
		return favorabilite_reglementation_change;
	}

	public void setFavorabilite_reglementation_change(Integer favorabilite_reglementation_change) {
		this.favorabilite_reglementation_change = favorabilite_reglementation_change;
	}

	public String getAutre_reglementation() {
		return autre_reglementation;
	}

	public void setAutre_reglementation(String autre_reglementation) {
		this.autre_reglementation = autre_reglementation;
	}

	public String getMesures_urgente() {
		return mesures_urgente;
	}

	public void setMesures_urgente(String mesures_urgente) {
		this.mesures_urgente = mesures_urgente;
	}

	public Integer getCorruption() {
		return corruption;
	}

	public void setCorruption(Integer corruption) {
		this.corruption = corruption;
	}

	public String isSituation_payer() {
		return situation_payer;
	}

	public void setSituation_payer(String situation_payer) {
		this.situation_payer = situation_payer;
	}

	public Integer getIncitation() {
		return incitation;
	}

	public void setIncitation(Integer incitation) {
		this.incitation = incitation;
	}

	

	public Double getResult() {
		return result;
	}

	public void setResult(Double result) {
		this.result = result;
	}

	//public static Finder<Integer,Regulation> find =new Finder<>(Regulation.class);
	public static Model.Finder<Long,Regulation> find = new Model.Finder<Long,Regulation>(Regulation.class);
	
}

